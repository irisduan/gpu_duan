package MonteCarlo;


import org.apache.commons.math3.distribution.NormalDistribution;

import java.util.Random;

public class NormalVectorGenerator implements RandomVectorGenerator{
	private int _number;
	
	public NormalVectorGenerator(int number){
		_number = number;
	}
	
	@Override
	public double[] getVector(){
		Random r = new Random();
		double[] vector = new double[_number];
		for ( int i = 0; i < vector.length; ++i){
			vector[i] = r.nextDouble();
		}
		return vector;
    }
	

}