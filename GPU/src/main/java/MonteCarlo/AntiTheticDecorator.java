package MonteCarlo;

import java.util.Arrays;

public class AntiTheticDecorator implements RandomVectorGenerator{
	protected RandomVectorGenerator _gen; 
	double[] lastVector;
	
	public AntiTheticDecorator( RandomVectorGenerator gen){
		this._gen = gen;
	}
	
	@Override
	public double[] getVector(){
		if (lastVector == null){
			lastVector = _gen.getVector();
			return lastVector;
		} else{
			double[] tmp =Arrays.copyOf(lastVector, lastVector.length);
			lastVector = null;
			for (int i = 0; i < tmp.length; ++i){ tmp[i] = -tmp[i];}
			return tmp;
		}
	}
}

