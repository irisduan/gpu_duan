package MonteCarlo;

/*
 * this class manages the simulation process, it takes 3 arguments
 * path generator, pay out calculator and statistics collector
 */


public class SimulationManager {
	private PayOut _payout;
	private StatCollector _sc;
	private StockPath _path;
	private double _discount; 
	private int _count;
	
	public SimulationManager(StockPath path, PayOut payout, double discount){
		_path = path;
		_payout = payout;
		_discount = discount;
		_count = 1000;
	}
	
	public double getPayOut(){
		_sc = new StatCollector(_path, _payout);
		double _mean = _sc.getMean();
		double _variance = _sc.getVariance(); 
		
//		System.out.println(_variance); //test
//		System.out.println(_mean);
		_count = (int) Math.round( _variance * Math.pow(1.75 * 100 / _mean, 2)) + 1;
//		System.out.println(_count); //test
		double _sum = 0 ;
		for(int i=0; i<_count; i++){
			_sum += _payout.getPayOut(_path);
		}
		return _sum / _count / _discount;
	}
}
