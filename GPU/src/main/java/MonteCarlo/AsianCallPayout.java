package MonteCarlo;
import java.util.List;

import org.apache.commons.math3.util.Pair;
import org.joda.time.DateTime;

public class AsianCallPayout implements PayOut{
	private double _strike;
	
	public AsianCallPayout(double strike){
		_strike = strike;
	}
	
	public double getPayOut(StockPath path) {
		//get the average of the price 
		List<Pair<DateTime, Double>> p = path.getPath();
		double sum = 0;
		for (int i = 0; i< p.size(); i++){
			double price = p.get(i).getSecond();
			sum +=price;
		}
		double avgPrice = sum/p.size();
		if(avgPrice > _strike) 
			return avgPrice - _strike;
		else return 0;
	}


}