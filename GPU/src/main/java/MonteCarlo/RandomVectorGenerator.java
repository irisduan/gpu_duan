package MonteCarlo;

public interface RandomVectorGenerator {
	public double[] getVector();
}
