package MonteCarlo;

public interface PayOut {
	    public double getPayOut(StockPath path);
}
