package MonteCarlo;

import java.util.ArrayList;
import java.util.DoubleSummaryStatistics;
import java.util.List;

import org.apache.commons.math3.util.Pair;
import org.joda.time.DateTime;

public class BrownianMotionPath implements StockPath{
	private double volatility;
	private double rate;
	private double currentPrice;
	private int timeLeft; 
	private List<Pair<DateTime,Double>> list;
	private RandomVectorGenerator nvg;
	private double[] antiVec;
	
	//constructor that takes 4 arguments 
	public BrownianMotionPath(double currentPrice, double volatility, double rate, int timeLeft){
        nvg = new AntiTheticDecorator(new GPUGaussianGenerator(252, 1024*256*16));
		this.currentPrice = currentPrice;
		this.volatility = volatility;
		this.rate = rate;
		this.timeLeft = timeLeft;
	}
	
	
	@Override  // implements the getPrice
	public List<Pair<DateTime,Double>> getPath(){
		 List<Pair<DateTime,Double>> list = new  ArrayList<Pair<DateTime,Double>>();
		 DateTime _currentDate = DateTime.now();
		 Double _currentPrice = currentPrice;
		 double[] normalVec = nvg.getVector();
		 for(int i = 0; i <timeLeft; i++){
			 _currentDate = _currentDate.plusDays(1);
			 _currentPrice = _currentPrice*Math.exp(rate - volatility*volatility/2 + volatility*normalVec[i]);
			 list.add(new Pair<DateTime,Double>(_currentDate,_currentPrice));
		 }
		 return list;
	}
}