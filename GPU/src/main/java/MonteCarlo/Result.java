package MonteCarlo;

import java.io.IOException;

public class Result {
	
	public static void main(String[] args) throws IOException {
		double discount = Math.pow(1+0.0001,252);
    	StockPath path = new BrownianMotionPath(152.35, 0.01, 0.0001, 252);	
    	
		PayOut AsianPayOut = new AsianCallPayout(164);
		SimulationManager asian_manager = new SimulationManager(path,AsianPayOut, discount);
		double _result2 = asian_manager.getPayOut();
		System.out.println("Runing Monte Carlo Simulation...");
		System.out.println("The Asian Call option price is " + _result2);
    	
 
		PayOut myPayOut = new EuropeanCallPayout(165);
		SimulationManager eur_manager = new SimulationManager(path, myPayOut, discount);
		double _result1 = eur_manager.getPayOut();	
		System.out.println("The European Call option price is " + _result1);
			
	}	
}