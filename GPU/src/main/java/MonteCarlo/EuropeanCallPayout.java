package MonteCarlo;

import java.util.List;

import org.apache.commons.math3.util.Pair;
import org.joda.time.DateTime;

public class EuropeanCallPayout implements PayOut{
	private double _strike;
	
	public EuropeanCallPayout(double strike){
		_strike = strike;
	}
	
	@Override
	public double getPayOut(StockPath path){
		//get the last price
		List<Pair<DateTime,Double>> p = path.getPath();
		Double lastPrice = p.get(p.size()-1).getSecond();
		if(lastPrice > _strike) return lastPrice-_strike;
		else return 0;
	}
}
