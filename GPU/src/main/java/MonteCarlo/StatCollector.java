package MonteCarlo;

import java.util.LinkedList;
import java.util.List;

/*
 * StatCollector decides when to stop the simulation process
 * it keeps track of the average and the standard deviation
 */
public class StatCollector {
	private double _mean;
	private double _variance;
	private double _sumOfSquare;
	private double _std;
	private double _sumOfPayout;
	private int _count; 
	private List<Double> _payout;
	
	public StatCollector(StockPath path, PayOut payout){
		update(path, payout, 1000);
	}
	
	public double getMean(){
		return _mean;
	}
	
	public double getVariance(){
		return _variance;
	}
	
	public double getSumOfSquare(){
		return _sumOfSquare;
	}
	
	public double getSumOfPayout(){
		return _sumOfPayout;
	}
	
	public int getCount(){
		return _count;
	}
	
	public double getStd(){
		return _std;
	}
	
	
	public void update(StockPath path, PayOut payout, int count){
		_sumOfPayout = 0;
		_sumOfSquare = 0;
		for(int i=0; i< count; i++){
			double _value = payout.getPayOut(path);
			_sumOfPayout += _value;
			_sumOfSquare += Math.pow(_value, 2);
		}
		_mean = _sumOfPayout/count;
		_variance = _sumOfSquare/count - Math.pow(_mean,2);
		_std = Math.sqrt(_variance);
	}

}
