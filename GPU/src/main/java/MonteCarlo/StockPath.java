package MonteCarlo;

import org.apache.commons.math3.util.Pair;
import org.joda.time.DateTime;
import java.util.List;

public interface StockPath {
	public List<Pair<DateTime, Double>> getPath();
}
